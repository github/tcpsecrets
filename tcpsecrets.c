#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/siphash.h>
#include <linux/ftrace.h>
#include <linux/version.h>
#include <net/tcp.h>
#include <net/net_namespace.h>
#include <net/secure_seq.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 13, 0)
#error "Kernel 4.13.0+ is required."
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,7,0)
#ifndef CONFIG_KPROBES
#error "KPROBES support not enabled in kernel."
#endif

#define USE_KPROBES
#include <linux/kprobes.h>

#else

#ifndef CONFIG_KALLSYMS
#error "KALLSYMS support not enabled in kernel."
#endif

#include <linux/kallsyms.h>
#endif

#define LOG_PREFIX "tcpsecrets: "
#define PROC_ENTRY "tcp_secrets"

typedef struct sock * (*cookie_v4_check_type)(
	struct sock *sk, struct sk_buff *skb);
typedef u32 (*secure_tcp_seq_type)(
	__be32 saddr, __be32 daddr, __be16 sport, __be16 dport);

static cookie_v4_check_type cookie_v4_check_ptr;
static secure_tcp_seq_type secure_tcp_seq_ptr;

static siphash_key_t (*syncookie_secret_ptr)[2] = NULL;
static siphash_key_t *net_secret_ptr = NULL;
static siphash_key_t *timestamp_secret_ptr = NULL;

static struct proc_dir_entry *proc_entry;

static void show_bytes(struct seq_file *m, const char *name,
			   const void *in, size_t size)
{
	size_t i;

	seq_printf(m, "%s ", name);
	for (i = 0; i < size; i++) {
		const u8 *bytes = (const u8 *)in;
		seq_printf(m, "%02x", (unsigned int)bytes[i]);
	}
	seq_printf(m, "\n");
}

static int tcp_secrets_show(struct seq_file *m, void *v)
{
	seq_printf(m, "time_sec %llu\n", ktime_get_real_seconds());
	seq_printf(m, "time_ms %llu\n", ktime_get_real_ns() / NSEC_PER_MSEC);
	seq_printf(m, "uptime_ms %u\n", tcp_time_stamp_raw());
	seq_printf(m, "jiffies %llu\n", get_jiffies_64());

	show_bytes(m, "cookie_secret",
		syncookie_secret_ptr, sizeof(*syncookie_secret_ptr));
	show_bytes(m, "net_secret",
		net_secret_ptr, sizeof(*net_secret_ptr));
	show_bytes(m, "timestamp_secret",
		timestamp_secret_ptr, sizeof(*timestamp_secret_ptr));

	return 0;
}

static int tcp_secrets_open(struct inode *inode, struct file *file)
{
	return single_open(file, tcp_secrets_show, NULL);
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 6, 0)
static const struct file_operations tcp_secrets_fops = {
	.open		= tcp_secrets_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};
#else
static const struct proc_ops tcp_secrets_fops = {
	.proc_open	= tcp_secrets_open,
	.proc_read	= seq_read,
	.proc_lseek	= seq_lseek,
	.proc_release	= single_release,
};
#endif

#ifdef USE_KPROBES
typedef unsigned long (*kallsyms_lookup_name_type)(const char *name);

static int dummy_kprobe_handler(struct kprobe *p, struct pt_regs *regs) {
	return 0;
}

static kallsyms_lookup_name_type find_kallsyms_lookup_name_symbol(void) {
	struct kprobe probe;
	int ret;
	kallsyms_lookup_name_type addr;

	memset(&probe, 0, sizeof(probe));
	probe.pre_handler = dummy_kprobe_handler;
	probe.symbol_name = "kallsyms_lookup_name";
	ret = register_kprobe(&probe);
	if (ret) {
		printk(LOG_PREFIX "register_kprobe() = %d", ret);
		return NULL;
	}
	addr = (kallsyms_lookup_name_type)probe.addr;
	unregister_kprobe(&probe);

	printk(LOG_PREFIX "register_kprobe() = %p", addr);
	return addr;
}
#endif

static unsigned long lookup_name(const char *name) {
#ifdef USE_KPROBES
	static kallsyms_lookup_name_type func_ptr = NULL;
	if (!func_ptr)
		func_ptr = find_kallsyms_lookup_name_symbol();

	return func_ptr(name);
#else
	return kallsyms_lookup_name(name);
#endif
}

static struct sock *cookie_v4_check_wrapper(struct sock *sk,
                                            struct sk_buff *skb)
{
	cookie_v4_check_type old_func =
		(void*)((unsigned long)cookie_v4_check_ptr + MCOUNT_INSN_SIZE);

	if (sock_net(sk)->ipv4.sysctl_tcp_syncookies == 2) {
		tcp_synq_overflow(sk);
	}
	return old_func(sk, skb);
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,11,0)
#define ftrace_regs pt_regs
static __always_inline struct pt_regs *ftrace_get_regs(struct ftrace_regs *fregs)
{
	return fregs;
}
#endif

static void notrace
tcpsecrets_ftrace_handler(unsigned long ip, unsigned long parent_ip,
			  struct ftrace_ops *fops, struct ftrace_regs *fregs)
{
	struct pt_regs *regs = ftrace_get_regs(fregs);
	regs->ip = (unsigned long)cookie_v4_check_wrapper;
}

static struct ftrace_ops tcpsecrets_ftrace_ops __read_mostly = {
	.func = tcpsecrets_ftrace_handler,
	.flags = FTRACE_OPS_FL_SAVE_REGS,
};

static void fix_cookie_v4_check(void)
{
	int ret;

	ret = ftrace_set_filter_ip(
		&tcpsecrets_ftrace_ops, (unsigned long)cookie_v4_check_ptr, 0, 0);
	if (ret)
		printk(LOG_PREFIX "can't set ftrace filter: err=%d\n", ret);

	ret = register_ftrace_function(&tcpsecrets_ftrace_ops);
	if (ret)
		printk(LOG_PREFIX "can't set ftrace function: err=%d\n", ret);
}

/* Force generation of secrets. */
static void init_secrets(void)
{
	struct iphdr ip = {};
	struct tcphdr tcp = {};
	struct in6_addr addr = {};
	u16 mss;

	/* syncookie_secret */
	__cookie_v4_init_sequence(&ip, &tcp, &mss);

	/* net_secret */
	secure_tcp_seq_ptr(0, 0, 0, 0);

	/* IPv4 version is not exported, but uses the same ts_secret.
	 * Addresses are passed as __be32*, but are used as IPv6.
	 */
	secure_tcpv6_ts_off(&init_net, (const __be32 *)&addr, (const __be32 *)&addr);
}

static int __init tcp_secrets_init(void)
{
	int rc;

	cookie_v4_check_ptr = lookup_name("cookie_v4_check");
	if (cookie_v4_check_ptr == NULL) {
		printk(LOG_PREFIX "no access to cookie_v4_check");
		return -ENXIO;
	}
	syncookie_secret_ptr = lookup_name("syncookie_secret");
	if (syncookie_secret_ptr == NULL) {
		printk(LOG_PREFIX "no access to syncookie_secret");
		return -ENXIO;
	}
	net_secret_ptr = lookup_name("net_secret");
	if (net_secret_ptr == NULL) {
		printk(LOG_PREFIX "no access to net_secret");
		return -ENXIO;
	}
	timestamp_secret_ptr = lookup_name("ts_secret");
	if (timestamp_secret_ptr == NULL) {
		printk(LOG_PREFIX "no access to ts_secret");
		return -ENXIO;
	}
	secure_tcp_seq_ptr = lookup_name("secure_tcp_seq");
	if (secure_tcp_seq_ptr == NULL) {
		printk(LOG_PREFIX "no access to secure_tcp_seq_ptr");
		return -ENXIO;
	}

	fix_cookie_v4_check();

	proc_entry = proc_create(PROC_ENTRY, 0, NULL, &tcp_secrets_fops);
	if (proc_entry == NULL) {
		printk(LOG_PREFIX "can't create /proc/" PROC_ENTRY "!\n");
		return -EIO;
	}

	init_secrets();
	return 0;
}
module_init(tcp_secrets_init);

static void __exit tcp_secrets_exit(void)
{
	int ret;

	if (cookie_v4_check_ptr) {
		ret = unregister_ftrace_function(&tcpsecrets_ftrace_ops);
		if (ret)
			printk(LOG_PREFIX "can't unregister ftrace\n");

		ret = ftrace_set_filter_ip(&tcpsecrets_ftrace_ops,
				(unsigned long)cookie_v4_check_ptr, 1, 0);
		if (ret)
			printk(LOG_PREFIX "can't unregister filter\n");

		cookie_v4_check_ptr = NULL;
	}

	syncookie_secret_ptr = NULL;
	net_secret_ptr = NULL;
	timestamp_secret_ptr = NULL;

	if (proc_entry) {
		remove_proc_entry(PROC_ENTRY, 0);
		proc_entry = NULL;
	}
}
module_exit(tcp_secrets_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Polyakov <apolyakov@beget.ru>");
MODULE_AUTHOR("Dmitry Kozlyuk <kozlyuk@bifit.com>");
MODULE_DESCRIPTION("Provide access to TCP SYN cookie secrets via /proc/" PROC_ENTRY);
MODULE_VERSION("2.7");
